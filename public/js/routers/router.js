var app = app || {};

(function () {
	'use strict';

	var RecipeRouter = Backbone.Router.extend({
		routes: {
			'*search': 'index'
		},

		index: function () {
			$(document).ready(function() {
				$('#search').keypress(function(event){
					if (event.keyCode == 13) {
						event.preventDefault();
						$('.js_search').click();
					}
				});
			});
		}
	});

	app.RecipeRouter = new RecipeRouter();
	Backbone.history.start();
})();
