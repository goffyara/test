/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	app.Recipe = Backbone.Model.extend({

		defaults: {
			id: '',
			category: '',
			recipe_text: '',
			recipe_title: ''
		},

		// Toggle the `completed` state of this todo item.
		toggle: function () {
			this.save({
				completed: !this.get('completed')
			});
		}
	});
})();
