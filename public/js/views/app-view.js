var app = app || {};

(function ($) {
    'use strict';

    app.AppView = Backbone.View.extend({

        el: '.recipeapp',

        events: {
            'click .js_clear': 'clear',
            'click .js_search': 'search',
        },

        initialize: function () {

            this.$list = $('.recipies-list');

            this.listenTo(app.Recipies, 'sync', this.addAll);
            this.listenTo(app.Recipies, 'sync', this.showNothing);
            this.listenTo(app.Recipies, 'search:results', this.showResult);
            this.listenTo(app.Recipies, 'all', this.render);
            app.Recipies.fetch({reset: true});
        },

        render: function (event) {
            console.log('this.render', event);
        },
        addOne: function (recipe) {
            var view = new app.RecipeView({ model: recipe });
            this.$list.append(view.render().el);
        },
        addAll: function () {
            this.$list.html('');
            app.Recipies.each(this.addOne, this);
        },
        search: function (event) {
            event.preventDefault();
            var $button = $(event.currentTarget),
                query = $button.parents('form').find('#search').val().replace(/[^A-Za-zА-Яа-яЁё-]/g, "");
                if (query.length < 1) {
                    this.clear(event);
                    return false;
                };

            app.Recipies.search(query);
        },
        showResult: function (results) {
            app.Recipies = results;
        },
        clear: function (event) {
            event.preventDefault();
            app.Recipies.fetch({reset: true});
        },
        showNothing: function() {
            var count = app.Recipies.length,
                $nothing = $('.js_nothing');
            if (count === 0) {
                $nothing.removeClass('hidden');
            } else {
                $nothing.addClass('hidden');
            }
        }
    });
})(jQuery);
