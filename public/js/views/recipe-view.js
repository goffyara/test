var app = app || {};

(function ($) {
	'use strict';

	app.RecipeView = Backbone.View.extend({
		tagName:  'div.recipe',

		template: _.template($('#recipe-template').html()),

		events: {
		},

		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},
	});
})(jQuery);
