var app = app || {};

(function () {
    'use strict';

    var Recipies = Backbone.Collection.extend({
        model: app.Recipe,
        url: '/info.json',

        search: function(query){
            this.searchTerm = query || '';
            this.fetch({
                url: '/info.json/' + this.searchTerm,
                reset: true,
                success: function(){
                    app.Recipies.trigger("search:results", app.Recipies);
                },
                error: function(collection, response){
                    app.Recipies.trigger("search:error", response);
                }
            });
        }
    });

    app.Recipies = new Recipies();
})();
