--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Category" (
    id integer NOT NULL,
    name character varying(80)
);


ALTER TABLE "Category" OWNER TO postgres;

--
-- Name: Recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Recipe" (
    id integer NOT NULL,
    recipe_text text,
    category_id integer,
    recipe_title character varying(120)
);


ALTER TABLE "Recipe" OWNER TO postgres;

--
-- Data for Name: Category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Category" (id, name) FROM stdin;
1	Булки
5	Батоны
3	Десерты
4	Соки
2	Воды
\.


--
-- Data for Name: Recipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Recipe" (id, recipe_text, category_id, recipe_title) FROM stdin;
7	Своими рецептами, как интересно и с пользой проводить свободное время, поделились друг с другом старшеклассники двенадцати средних школ Железнодорожного округа Курска . А вот как правильно об этом донести информацию до остальных школьников, да еще сделать это достаточно креативно и увлекательно, старшеклассникам рассказали студенты Академии госслужбы. Анастасия Нехорошева и Денис Проскурин провели настоящий мастер-класс, где на примере просветительского проекта &quot;Шаг навстречу!&quot; (автор Александр Губанов) продемонстрировали свои организаторские и просветительские умения. 	1	Рецепт свободного времени
6	Готовь булки Вкусные домашние десерты — это гордость хозяйки и настоящее украшение стола. Но часто за повседневной готовкой первого, второго, третьего и компота, не остается ни сил, ни вдохновения на готовку десертов. Кажется, что это долго, трудно или нужно много разных ингредиентов. Можно приготовить традиционные домашние сладости — печенье, сладкие булочки и даже торт. Но ведическая кулинария рассказывает нам о других интересных и простых рецептах домашних сладостей. В предыдущих статьях я уже рассказывала про сливочные конфеты бурфи и про домашний шоколад. так	4	Мясо отварить, бульон от мяса процедить.
8	Этот рецепт уже давно живет в нашей семье. Готовила много раз и в виде торта и в виде пирожных. А вот теперь решила приготовить капкейки. Нежнейшая, влажная, шоколадная основа и сливочно-клубничный крем чудесно дополняют друг друга. А бальзамический уксус придает особый вкусовой оттенок. Все очень гармонично и нежно.	1	Шоколадные капкейки с бальзамическим уксусом 
9	Очень простой пирог, но в то же время очень вкусный. Влажный и очень нежный. Мои дети любят его просто так. Но а если добавить ложечку любимого варенья или шарик мороженого, то вполне можно подать и на праздничный стол. Рецепт из интернета. 	3	Неаполиатанский пирог с манкой и рикоттой 
\.


--
-- Name: PK1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Category"
    ADD CONSTRAINT "PK1" PRIMARY KEY (id);


--
-- Name: PK2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Recipe"
    ADD CONSTRAINT "PK2" PRIMARY KEY (id);


--
-- Name: FK1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Recipe"
    ADD CONSTRAINT "FK1" FOREIGN KEY (category_id) REFERENCES "Category"(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

