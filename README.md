Test Recipies Project
=======================

Introduction
------------
* Test Recipies Project
* Admin Path: http://app.localhost/admin
* Login: admin
* Password: admin

### Installation

    git clone https://bitbucket.org/goffyara/test.git /path
    cd /path
    curl -s https://getcomposer.org/installer | php
    php composer.phar global require "fxp/composer-asset-plugin:~1.1"
    php composer.phar install
    psql dbname < /path/data/dump/test.sql

    Replace db conect settings in /path/config/autoload/global.php

Web server setup
----------------

### Apache setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName app.localhost
        DocumentRoot /path/public
        <Directory /path/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
            <IfModule mod_authz_core.c>
            Require all granted
            </IfModule>
        </Directory>
    </VirtualHost>

### Nginx setup

To setup nginx, open your `/path/to/nginx/nginx.conf` and add an
[include directive](http://nginx.org/en/docs/ngx_core_module.html#include) below
into `http` block if it does not already exist:

    http {
        # ...
        include sites-enabled/*.conf;
    }


Create a virtual host configuration file for your project under `/path/to/nginx/sites-enabled/app.localhost.conf`
it should look something like below:

    server {
        listen       80;
        server_name  app.localhost;
        root         /path/public;

        location / {
            index index.php;
            try_files $uri $uri/ @php;
        }

        location @php {
            # Pass the PHP requests to FastCGI server (php-fpm) on 127.0.0.1:9000
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_param  SCRIPT_FILENAME /path/public/index.php;
            include fastcgi_params;
        }
    }

Restart the nginx, now you should be ready to go!
