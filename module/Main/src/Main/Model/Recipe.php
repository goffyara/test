<?php
namespace Main\Model;

use Zend\Validator\Digits;
use Zend\Validator\StringLength;

class Recipe
{
    protected $CategoryTable;
    public $id;
    public $recipe_text;
    public $recipe_title;
    public $category_id;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->recipe_text = (!empty($data['recipe_text'])) ? $data['recipe_text'] : null;
        $this->recipe_title = (!empty($data['recipe_title'])) ? $data['recipe_title'] : null;
        $this->category_id = (!empty($data['category_id'])) ? $data['category_id'] : null;
    }

    public function validate($scenario) {
        $validator_title = new StringLength(array('max' => 120, 'min' => 1));
        if(!$validator_title->isValid($this->recipe_title)) {
                return false;
        }
        $validator_text = new StringLength(array('max' => 3000, 'min' => 1));
        if(!$validator_text->isValid($this->recipe_text)) {
                return false;
        }
        if (!$this->validateId($this->category_id)) {
                return false;
            }

        if ($scenario == 'update') {
            if (!$this->validateId($this->id)) {
                return false;
            }
        }
        return true;
    }

    private function validateId($id)
    {
        $validator_id = new StringLength(array('min' => 1));
        if(!$validator_id->isValid($id)) {
            return false;
        }
        $validator_dig = new Digits();
        if(!$validator_dig->isValid($id)) {
            return false;
        }
        return true;
    }
}
