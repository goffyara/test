<?php
namespace Main\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class RecipeTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getRecipe($id)
    {
        $id     = (int) $id;
        $rowset = $this->tableGateway->select(array(
            'id' => $id
        ));
        $row    = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function createRecipe(Recipe $Recipe)
    {
        $data = array(
            'id' => $this->getMaxId() + 1,
            'recipe_text' => $Recipe->recipe_text,
            'category_id' => $Recipe->category_id,
            'recipe_title' => $Recipe->recipe_title
        );
        $this->tableGateway->insert($data);
    }

    public function saveRecipe(Recipe $Recipe)
    {
        $data = array(
            'recipe_text' => $Recipe->recipe_text,
            'category_id' => $Recipe->category_id,
            'recipe_title' => $Recipe->recipe_title
        );

        $id = (int) $Recipe->id;
        if ($id === 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRecipe($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception('Recipe id does not exist');
            }
        }
    }

    public function deleteRecipe($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }

    public function searchRecipe($query)
    {
        $where = new Where();
        $where->like('recipe_text', "%$query%")
            ->or
            ->like('recipe_title', "%$query%");

        $resultSet = $this->tableGateway->select($where);
        return $resultSet;
    }

    public function getMaxId()
{
    $select = $this->tableGateway->getSql()->select();
    $select->columns(array(
        'id' => new Expression('MAX(id)')
    ));

    $rowset = $this->tableGateway->selectWith($select);
    $row = $rowset->current();
    if (!$row) {
        throw new \Exception("Could not retrieve max id");
    }

    return $row->id;
}
}
