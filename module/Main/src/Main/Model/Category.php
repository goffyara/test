<?php
namespace Main\Model;
use Zend\Validator\Digits;
use Zend\Validator\StringLength;

 class Category
 {
     public $id;
     public $name;

     public function exchangeArray($data)
     {
         $this->id = (!empty($data['id'])) ? $data['id'] : null;
         $this->name = (!empty($data['name'])) ? $data['name'] : null;
     }

     public function validate($scenario) {
        $validator_name = new StringLength(array('max' => 50, 'min' => 1));
        if(!$validator_name->isValid($this->name)) {
                return false;
        }
        if ($scenario == 'update') {
            $validator_id = new StringLength(array('min' => 1));
            if(!$validator_id->isValid($this->id)) {
                return false;
            }
            $validator_dig = new Digits();
            if(!$validator_dig->isValid($this->id)) {
                return false;
            }
        }
        return true;
    }
 }
