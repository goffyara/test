<?php
namespace Main\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class CategoryTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getCategory($id)
    {
        $id     = (int) $id;
        $rowset = $this->tableGateway->select(array(
            'id' => $id
        ));
        $row    = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function createCategory(Category $Category)
    {
        $data = array(
            'id' => $this->getMaxId() + 1,
            'name' => $Category->name
        );
        $this->tableGateway->insert($data);
    }

    public function saveCategory(Category $Category)
    {
        $data = array(
            'name' => $Category->name,
        );

        $id = (int) $Category->id;
        if ($id === 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getCategory($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception('Category id does not exist');
            }
        }
    }

    public function deleteCategory($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }

    public function getMaxId()
{
    $select = $this->tableGateway->getSql()->select();
    $select->columns(array(
        'id' => new Expression('MAX(id)')
    ));

    $rowset = $this->tableGateway->selectWith($select);
    $row = $rowset->current();
    if (!$row) {
        throw new \Exception("Could not retrieve max id");
    }

    return $row->id;
}
}
