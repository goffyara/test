<?php

namespace Main\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;

/**
 *
 */
class InfoController extends AbstractRestfulController
{
	protected $RecipeTable;
	protected $CategoryTable;
	/**
	 * Return list of resources
	 *
	 * @return array
	 */
	public function getList()
	{
		$data = null;

		$recipies = $this->getRecipeTable()->fetchAll();
		foreach ($recipies as $key => $Recipe) {
			$Category = $this->getCategoryTable()->getCategory($Recipe->category_id);
			$data[] = array(
				'id' => $Recipe->id,
				'recipe_text' => $Recipe->recipe_text,
				'category' => $Category->name,
				'recipe_title' => $Recipe->recipe_title
			);
		}

		return $data;
	}

	/**
	 * Return search resource
	 *
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id)
	{
		$data = null;
		$recipies = $this->getRecipeTable()->searchRecipe($id);

		foreach ($recipies as $key => $Recipe) {
			$Category = $this->getCategoryTable()->getCategory($Recipe->category_id);
			$data[] = array(
				'id' => $Recipe->id,
				'recipe_text' => $Recipe->recipe_text,
				'category' => $Category->name,
				'recipe_title' => $Recipe->recipe_title
			);
		}

		return $data;
	}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data)
	{}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data)
	{}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id)
	{}

	public function getRecipeTable()
	{
		if (!$this->RecipeTable) {
			$sm = $this->getServiceLocator();
			$this->RecipeTable = $sm->get('Main\Model\RecipeTable');
		}
		return $this->RecipeTable;
	}

		public function getCategoryTable()
	{
		if (!$this->CategoryTable) {
			$sm = $this->getServiceLocator();
			$this->CategoryTable = $sm->get('Main\Model\CategoryTable');
		}
		return $this->CategoryTable;
	}
}
