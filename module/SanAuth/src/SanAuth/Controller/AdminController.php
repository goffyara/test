<?php

namespace SanAuth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Main\Model\Recipe;
use \Exception;

class AdminController extends AbstractActionController
{
    protected $RecipeTable;
    protected $CategoryTable;

    public function indexAction()
    {
        $this->checkAdmin();
        $categories = $this->getCategoryTable()->fetchAll();

        return new ViewModel(array('categories' => $categories, 'messages' => $this->flashmessenger()->getMessages()));
    }

    public function editAction()
    {
        $this->checkAdmin();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $recipeForm = $request->getPost('recipe', null);
            if (!empty($recipeForm)) {
                $this->escapeHtml($recipeForm);
                $Recipe = $this->getServiceLocator()->get('Recipe');
                $Recipe->id = $recipeForm['id'];
                $Recipe->recipe_text = $recipeForm['recipe_text'];
                $Recipe->category_id = $recipeForm['category_id'];
                $Recipe->recipe_title = $recipeForm['recipe_title'];
                if ($Recipe->validate('update')) {
                    $this->getRecipeTable()->saveRecipe($Recipe);
                    return $this->redirect()->toRoute('admin');
                } else {
                    $this->flashmessenger()->addMessage('Поля не могут быть пустыми.');
                    $this->flashmessenger()->addMessage('Количество символов в поле Текст рецепта не должно превышать 3000!');
                    $this->flashmessenger()->addMessage('Количество символов в поле Заголовок рецептане должно превышать 120!');
                    return $this->redirect()->toUrl('/admin/edit?id=' . $Recipe->id);
                }
            }
        }

        $id = $request->getQuery('id', null);

        if (empty($id)) {
            return $this->redirect()->toRoute('admin');
        }

        $Recipe = $this->getRecipeTable()->getRecipe($id);
        $categories = $this->getCategoryTable()->fetchAll();

        if (empty($Recipe)) {
            return $this->redirect()->toRoute('admin');
        }

        $data = array(
            'id' => $Recipe->id,
            'recipe_text' => $Recipe->recipe_text,
            'category_id' => $Recipe->category_id,
            'recipe_title' => $Recipe->recipe_title
        );

        return new ViewModel(array(
            'data' => $data,
            'categories' => $categories,
            'messages' => $this->flashmessenger()->getMessages()
        ));
    }

    public function deleteAction()
    {
        $this->checkAdmin();
        $request = $this->getRequest();
        $id = $request->getQuery('id', null);

        if (empty($id)) {
            return $this->redirect()->toRoute('admin');
        }

        $this->getRecipeTable()->deleteRecipe($id);

        return $this->redirect()->toRoute('admin');
    }

    public function createAction()
    {
        $this->checkAdmin();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $recipeForm = $request->getPost('recipe', null);
            if (!empty($recipeForm)) {
                $this->escapeHtml($recipeForm);
                $Recipe = $this->getServiceLocator()->get('Recipe');
                $Recipe->recipe_text = $recipeForm['recipe_text'];
                $Recipe->category_id = $recipeForm['category_id'];
                $Recipe->recipe_title = $recipeForm['recipe_title'];
                if ($Recipe->validate('create')) {
                    $this->getRecipeTable()->createRecipe($Recipe);
                } else {
                    $this->flashmessenger()->addMessage('Поля не могут быть пустыми.');
                    $this->flashmessenger()->addMessage('Количество символов в поле Текст рецепта не должно превышать 3000!');
                    $this->flashmessenger()->addMessage('Количество символов в поле Заголовок рецептане должно превышать 120!');
                }
            }
        }

        return $this->redirect()->toRoute('admin');
    }

    public function categoryAction()
    {
        $this->checkAdmin();
        $categories = $this->getCategoryTable()->fetchAll();
        return new ViewModel(
            array('categories' => $categories, 'messages' => $this->flashmessenger()->getMessages())
        );

    }
    public function getRecipeTable()
    {
        if (!$this->RecipeTable) {
            $sm = $this->getServiceLocator();
            $this->RecipeTable = $sm->get('Main\Model\RecipeTable');
        }
        return $this->RecipeTable;
    }

    public function getCategoryTable()
    {
        if (!$this->CategoryTable) {
            $sm = $this->getServiceLocator();
            $this->CategoryTable = $sm->get('Main\Model\CategoryTable');
        }
        return $this->CategoryTable;
    }

    private function checkAdmin() {
        $isAdmin = $this->getServiceLocator()->get('AuthService')->hasIdentity();
        if (!$isAdmin){
            return $this->redirect()->toRoute('login');
        }
        $this->layout()->setVariable('isAdmin', $isAdmin);
    }

    private function escapeHtml(&$form) {
        $escapeHtml = $this->getServiceLocator()->get('ViewHelperManager')->get('escapeHtml');
        foreach ($form as $key => &$value) {
            $escapeHtml($value);
        }
    }
}
