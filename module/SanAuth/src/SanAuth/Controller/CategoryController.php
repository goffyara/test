<?php

namespace SanAuth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Main\Model\Category;
use Zend\Validator\StringLength;
use \Exception;

class CategoryController extends AbstractActionController
{
    private $messages;
    protected $CategoryTable;

    public function editAction()
    {
        $this->checkAdmin();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryForm = $request->getPost('category', null);
            if (!empty($categoryForm)) {
                $categoryForm = $this->escapeHtml($categoryForm);
                $viewHelperManager = $this->getServiceLocator()->get('ViewHelperManager');
                $escapeHtml = $viewHelperManager->get('escapeHtml');
                $Category = $this->getServiceLocator()->get('Category');
                $Category->id = $escapeHtml($categoryForm['id']);
                $Category->name = $escapeHtml($categoryForm['name']);
                if ($Category->validate('update')) {
                    $this->getCategoryTable()->saveCategory($Category);
                }
            }
        }

        return $this->redirect()->toUrl('/admin/category');
    }

    public function deleteAction()
    {
        $this->checkAdmin();
        $request = $this->getRequest();
        $id = $request->getQuery('id', null);

        if (empty($id)) {
            return $this->redirect()->toUrl('/admin/category');
        }
        try {
            $this->getCategoryTable()->deleteCategory($id);
        } catch (Exception $e) {
            $this->flashmessenger()->addMessage('Нельзя удалять при наличии рецептов этой категории!');
            return $this->redirect()->toUrl('/admin/category');
        }

        return $this->redirect()->toUrl('/admin/category');
    }

    public function createAction()
    {
        $this->checkAdmin();
        $isAdmin = $this->getServiceLocator()->get('AuthService')->hasIdentity();
        if (!$isAdmin){
            return $this->redirect()->toRoute('login');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryForm = $request->getPost('category', null);
            if (!empty($categoryForm)) {
                $this->escapeHtml($categoryForm);
                $Category = $this->getServiceLocator()->get('Category');
                $Category->name = $categoryForm['name'];
                if ($Category->validate('create')) {
                    $this->getCategoryTable()->createCategory($Category);
                } else {
                    $this->flashmessenger()->addMessage('Поле названия не может быть пустым. Количество символов не должно превышать 50!');
                }
            }
        }

        return $this->redirect()->toUrl('/admin/category');
    }

    public function getCategoryTable()
    {
        if (!$this->CategoryTable) {
            $sm = $this->getServiceLocator();
            $this->CategoryTable = $sm->get('Main\Model\CategoryTable');
        }
        return $this->CategoryTable;
    }

    private function checkAdmin() {
        $isAdmin = $this->getServiceLocator()->get('AuthService')->hasIdentity();
        if (!$isAdmin){
            return $this->redirect()->toRoute('login');
        }
    }

    private function escapeHtml(&$categoryForm) {
        $escapeHtml = $this->getServiceLocator()->get('ViewHelperManager')->get('escapeHtml');
        foreach ($categoryForm as $key => &$value) {
            $escapeHtml($value);
        }
    }
}
