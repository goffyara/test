<?php

namespace SanAuth;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\Digest as AuthAdapter;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories'=>array(
                'SanAuth\Model\MyAuthStorage' => function ($sm) {
                    return new \SanAuth\Model\MyAuthStorage('trololo');
                },

                'AuthService' => function ($sm) {
                    $config = $sm->get('Configuration');
                    $authFilePath = $config['authFilePath'];
                    if (!file_exists($authFilePath)) {
                        throw new \Exception("Файл авторизации не найден по пути --> $authFilePath");
                    }
                    $AuthAdapter  = new AuthAdapter($authFilePath, 'Admin');

                    $authService = new AuthenticationService();
                    $authService->setAdapter($AuthAdapter);
                    $authService->setStorage($sm->get('SanAuth\Model\MyAuthStorage'));

                    return $authService;
                },
            ),
        );
    }

}
